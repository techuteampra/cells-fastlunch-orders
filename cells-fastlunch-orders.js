{
  const {
    html,
  } = Polymer;
  /**
    `<cells-fastlunch-orders>` Description.

    Example:

    ```html
    <cells-fastlunch-orders></cells-fastlunch-orders>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-fastlunch-orders | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsFastlunchOrders extends Polymer.mixinBehaviors([CellsBehaviors.i18nBehavior,], Polymer.Element) {
    static get is() {
      return 'cells-fastlunch-orders';
    }

    static get properties() {
      return {
        orderList: {
          type: Array,
          observer: 'init'
        },
        orderListFormated: {
          type: Array
        },
        orderSelected: {
          type: Object
        }
      };

    }

    init() {
      console.log("_formatOrders");
      let orderListTemp = [];

      this.orderList.forEach(item => {
        let descProducts = "";
        let imgPedidoCombinada = "";
        let descEstadoPedido = "Registrado";
        let descTipoPago = "Tarjeta de Crédito";

        item.orderProds.forEach((itemProducts, index) => {
          descProducts = ((index == 0) ? itemProducts.nombre : (descProducts + " + " + itemProducts.nombre));
          imgPedidoCombinada = ((index == 0) ? itemProducts.nombreImg : "https://comedor.s3-sa-east-1.amazonaws.com/productos/combo.png");
        });

        if (item.estado == 'E') {
          descEstadoPedido = "Entregado";
        } else if (item.estado == 'X') {
          descEstadoPedido = "Anulado";
        }

        if (item.tipoPago == "LK") {
          descTipoPago = "Lukita"
        }

        let orderFormat = {
          pedidoId: item.idPedido,
          fechaPedido: item.fechaRegistro,
          estadoPedido: descEstadoPedido,
          tipoPagoPedido: item.tipoPago,
          descTipoPagoPedido: descTipoPago,
          horarioAsignado: item.horarioAsignado,
          imgSrc: imgPedidoCombinada,
          name: item.idPedido,
          description: {
            masked: false,
            content: descProducts
          },
          //item.estado
          primaryAmount: {
            label: descEstadoPedido,
            amount: item.montoTotal,
            currency: 'S/'
          }

        }
        orderListTemp.push(orderFormat);
      });

      this.set('orderListFormated', orderListTemp);

      console.log("orderListFormated", this.orderListFormated);
    }

    _verDetalleModal(e) {
      let rowDiv = e.currentTarget;
      let indexData = rowDiv.getAttribute('data-index');
      console.log(indexData);

      this.orderListFormated.forEach((item, index) => {
        if (index == indexData) {
          let orderSelected = {
            pedidoId: item.pedidoId,
            montoSel: item.primaryAmount.amount,
            monedaSel: item.primaryAmount.currency,
            imgSrc: item.imgSrc,
            nameSel: item.name,
            descriptionSel: item.description.content,
            estadoPedidoSel: item.primaryAmount.label,
            fechaPedidoSel: item.fechaPedido,
            horarioAsigSel: item.horarioAsignado,
            tipoPagoSel: item.descTipoPagoPedido,
            usuarioId: item.idUsuario
          }

          this.orderSelected = orderSelected;
          console.log(orderSelected);
        }

      });

      this.shadowRoot.querySelector('cells-middle-modal').set('open', true);

    }

    setIntervalX(callback, delay, repetitions) {
      var x = 0;
      var intervalID = window.setInterval(function () {
        callback();
        if (++x === repetitions) {
          window.clearInterval(intervalID);
        }
      }, delay);
    }

    setFadeOrder(numItem){
      let elem = this.shadowRoot.getElementById(numItem + '_pedidoItem').classList.toggle("fade");
      this.setIntervalX(function(){elem}, 1000, 3);
    }

  }

  customElements.define(CellsFastlunchOrders.is, CellsFastlunchOrders);
}